Resumen

(Indica el resumen de la incidencia)

Pasos a replicar

(Indica paso a paso para poder replicar el error)

¿Cuál es el comportamiento actual?

(Indica el estado actual)

¿Cuál es el comportamiento esperado?

(Indica el comportamiento esperado)